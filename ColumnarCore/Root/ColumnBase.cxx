/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarCore/ColumnBase.h>

#include <AsgDataHandles/VarHandleKey.h>
#include <ColumnarInterfaces/ColumnInfo.h>

//
// method implementations
//

namespace col
{
  ColumnBaseImp<ColumnarModeXAOD> ::
  ColumnBaseImp ()
  {
  }

  ColumnBaseImp<ColumnarModeXAOD> ::
  ~ColumnBaseImp ()
  {
  }

  StatusCode ColumnBaseImp<ColumnarModeXAOD> ::
  initializeColumns ()
  {
    for (auto *key : m_keys)
    {
      if (key->initialize().isFailure())
        return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }

  void ColumnBaseImp<ColumnarModeXAOD> ::
  callEvents (ObjectRange<ObjectType::event,ColumnarModeXAOD> /*events*/) const
  {
    throw std::runtime_error ("tool didn't implement callEvents");
  }

  ColumnBaseImp<ColumnarModeDirect> ::
  ColumnBaseImp ()
  {
  }

  ColumnBaseImp<ColumnarModeDirect> ::
  ~ColumnBaseImp ()
  {
  }

  StatusCode ColumnBaseImp<ColumnarModeDirect> ::
  initializeColumns ()
  {
    return StatusCode::SUCCESS;
  }



  void ColumnBaseImp<ColumnarModeDirect> ::
  callEvents (ObjectRange<ObjectType::event,ColumnarModeDirect> /*events*/) const
  {
    throw std::runtime_error ("tool didn't implement callEvents");
  }



  const std::string& ColumnBaseImp<ColumnarModeDirect> ::
  objectName (ObjectType objectType) const
  {
    auto iter = m_objectNames.find (objectType);
    if (iter == m_objectNames.end())
      throw std::runtime_error ("object type not registered, make sure to register object handle first: " + std::to_string (unsigned(objectType)));
    return iter->second;
  }



  void ColumnBaseImp<ColumnarModeDirect> ::
  setObjectName (ObjectType objectType, const std::string& name)
  {
    auto [iter, success] = m_objectNames.emplace (objectType, name);
    if (!success && iter->second != name)
      throw std::runtime_error ("object type already registered with different name: " + name + " vs " + iter->second);
  }

  void ColumnBaseImp<ColumnarModeDirect> ::
  checkColumnsValid () const
  {
    for (auto& column : columns)
    {
      if (*column.second.dataPtr == nullptr)
        throw std::runtime_error ("uninitialized column: " + column.first);
    }
    std::optional<std::size_t> eventCount;
    for (auto& column : columns)
    {
      if (column.second.offsets != nullptr)
      {
        auto& offsets = *column.second.offsets;
        if (*offsets.second.sizePtr == 0u)
          throw std::runtime_error ("empty offset column: " + offsets.first);
        auto *offsetsPtr = static_cast<const ColumnarOffsetType*>
          (*offsets.second.dataPtr);
        if (*column.second.sizePtr != offsetsPtr[*offsets.second.sizePtr-1] + column.second.extraMembers)
          throw std::runtime_error ("column size doesn't match offset column: " + column.first);
      } else
      {
        if (column.first == "EventInfo")
        {
          if (*column.second.sizePtr != 1u)
            throw std::runtime_error ("EventInfo offset column needs to be exactly length 1");
          if (!eventCount.has_value())
            eventCount = static_cast<const ColumnarOffsetType*> (*column.second.dataPtr)[0];
          else if (eventCount.value() != static_cast<const ColumnarOffsetType*> (*column.second.dataPtr)[0])
            throw std::runtime_error ("inconsistent size for EventInfo offset column: " + std::to_string (eventCount.value()) + " vs " + std::to_string (static_cast<const ColumnarOffsetType*> (*column.second.dataPtr)[0]));
        } else
        {
          if (!eventCount.has_value())
            eventCount = *column.second.sizePtr-column.second.extraMembers;
          else if (eventCount.value() + column.second.extraMembers != *column.second.sizePtr)
            throw std::runtime_error ("inconsistent size for columns without offsets: " + column.first + " " + std::to_string (eventCount.value()) + " + " + std::to_string (column.second.extraMembers) + " vs " + std::to_string (*column.second.sizePtr));
        }
      }
    }
  }

  std::vector<std::string> ColumnBaseImp<ColumnarModeDirect> ::
  getColumnNames () const
  {
    std::vector<std::string> result;
    for (auto& column : columns)
      result.push_back (column.first);
    return result;
  }




  ColumnBaseImp<ColumnarModeExternal> ::
  ColumnBaseImp ()
  {
    addColumn<ColumnarOffsetType> (numberOfEventsName, m_eventsIndex, "", 1u);
  }

  ColumnBaseImp<ColumnarModeExternal> ::
  ~ColumnBaseImp ()
  {
  }

  StatusCode ColumnBaseImp<ColumnarModeExternal> ::
  initializeColumns ()
  {
    return StatusCode::SUCCESS;
  }



  void ColumnBaseImp<ColumnarModeExternal> ::
  callVoid (void **data) const
  {
    auto eventsIter = columns.find (numberOfEventsName);
    if (eventsIter == columns.end())
      throw std::runtime_error ("no EventInfo column defined");
    if (*eventsIter->second.type != typeid (ColumnarOffsetType))
      throw std::runtime_error ("EventInfo column has wrong type");
    auto eventsOffset = static_cast<const ColumnarOffsetType*> (data[*eventsIter->second.dataIndexPtr.front()]);
    callEvents (ObjectRange<ObjectType::event,ColumnarModeExternal> (data, eventsOffset[0], eventsOffset[1]));
  }



  std::vector<ColumnInfo> ColumnBaseImp<ColumnarModeExternal> ::
  getColumnInfo () const
  {
    std::vector<ColumnInfo> result;
    for (auto& column : columns)
    {
      ColumnInfo info;
      info.name = column.first;
      info.index = *column.second.dataIndexPtr.front();
      info.type = column.second.type;
      info.accessMode = column.second.isConst ? ColumnAccessMode::input : ColumnAccessMode::output;
      info.offsetName = column.second.offsets != nullptr ? column.second.offsets->first : "";
      info.isOffset = bool (column.second.extraMembers != 0u);
      result.push_back (info);
    }
    return result;
  }



  void ColumnBaseImp<ColumnarModeExternal> ::
  callEvents (ObjectRange<ObjectType::event,ColumnarModeExternal> /*events*/) const
  {
    throw std::runtime_error ("tool didn't implement callEvents");
  }



  const std::string& ColumnBaseImp<ColumnarModeExternal> ::
  objectName (ObjectType objectType) const
  {
    auto iter = m_objectNames.find (objectType);
    if (iter == m_objectNames.end())
      throw std::runtime_error ("object type not registered, make sure to register object handle first: " + std::to_string (unsigned(objectType)));
    return iter->second;
  }



  void ColumnBaseImp<ColumnarModeExternal> ::
  setObjectName (ObjectType objectType, const std::string& name)
  {
    auto [iter, success] = m_objectNames.emplace (objectType, name);
    if (!success && iter->second != name)
      throw std::runtime_error ("object type already registered with different name: " + name + " vs " + iter->second);
  }
}
