/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_READ_OBJECT_HANDLE_H
#define COLUMNAR_PROTOTYPE_READ_OBJECT_HANDLE_H

#include <ColumnarCore/ColumnBase.h>
#include <ColumnarCore/ObjectId.h>
#include <ColumnarCore/ObjectRange.h>
#include <ColumnarCore/ObjectType.h>
#include <assert.h>

#include <AsgDataHandles/ReadHandleKey.h>

namespace col
{
  /// @brief a class for read access to an object in the event store
  template<ObjectType O,typename CM=ColumnarModeDefault> class ReadObjectHandle;




  template<ObjectType O> class ReadObjectHandle<O,ColumnarModeXAOD> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeXAOD;

    ReadObjectHandle (ColumnBaseImp<CM>& columnBase,
                      const std::string& name)
      : m_columnBase (&columnBase),
        m_key (name)
    {
      columnBase.addKey (&m_key);
    }

    template<typename U>
    ReadObjectHandle (U& owner,
                      const std::string& name,
                      const std::string& propertyName, const std::string& propertyTitle)
      : ReadObjectHandle (owner, name)
    {
      owner.declareProperty (propertyName, m_key, propertyTitle);
    }

    ObjectId<O,CM> getSingleObject () const
    {
      static_assert (O == ObjectType::event, "only supported for EventInfo handles");
      const typename detail::ObjectTypeTraits<O>::xAODObject *object = nullptr;
      if (!m_columnBase->eventStore().retrieve (object, name()).isSuccess())
        throw std::runtime_error ("failed to retrieve " + name());
      return ObjectId<O,CM> (object);
    }

    ObjectRange<O,CM> getFullRange () const
    {
      const typename detail::ObjectTypeTraits<O>::xAODContainer *container = nullptr;
      if (!m_columnBase->eventStore().retrieve (container, name()).isSuccess())
        throw std::runtime_error ("failed to retrieve " + name());
      return ObjectRange<O,CM> (container);
    }

    ObjectRange<O,CM> getRange (ObjectId<ObjectType::event,CM> /*eventId*/) const
    {
      return getFullRange ();
    }

    ObjectRange<O,CM> getRange (ObjectRange<ObjectType::event,CM> eventRange) const
    {
      if (eventRange.beginIndex() == eventRange.endIndex())
        return ObjectRange<O> (getFullRange().getContainer(), 0u, 0u);
      return getFullRange ();
    }

    const std::string& name () const noexcept
    {
      return m_key.key();
    }

    ColumnBaseImp<CM> *columnBase () noexcept {
      return m_columnBase;}



    /// Private Members
    /// ===============
  private:

    ColumnBaseImp<CM> *m_columnBase = nullptr;
    SG::ReadHandleKey<typename detail::ObjectTypeTraits<O>::xAODStoreType> m_key;
  };






  template<ObjectType O> class ReadObjectHandle<O,ColumnarModeDirect> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeDirect;

    ReadObjectHandle (ColumnBaseImp<CM>& columnBase,
                      const std::string& name)
      : m_columnBase (&columnBase),
        m_name (name)
    {
      columnBase.setObjectName (O, name);
      columnBase.addColumn (name, m_size, m_offsets, "", 1u);
    }

    template<typename U>
    ReadObjectHandle (U& owner,
                      const std::string& name,
                      const std::string& propertyName, const std::string& propertyTitle)
      : ReadObjectHandle (owner, name)
    {
    }

    ObjectRange<O,CM> getFullRange () const
    {
      return ObjectRange<O,CM> (0u, m_offsets[m_size-1]);
    }

    ObjectRange<O,CM> getRange (ObjectId<ObjectType::event,CM> eventId) const
    {
      assert (eventId.getIndex() < m_size);
      return ObjectRange<O,CM> (m_offsets[eventId.getIndex()], m_offsets[eventId.getIndex() + 1]);
    }

    ObjectRange<O,CM> getRange (ObjectRange<ObjectType::event,CM> eventRange) const
    {
      assert (eventRange.endIndex() <= m_size);
      return ObjectRange<O,CM> (m_offsets[eventRange.beginIndex()], m_offsets[eventRange.endIndex()]);
    }

    const std::string& name () const noexcept
    {
      return m_name;
    }

    const ColumnarOffsetType *const* offsetsPtr () const noexcept {
      return &m_offsets;}

    ColumnBaseImp<CM> *columnBase () noexcept {
      return m_columnBase;}



    /// Private Members
    /// ===============
  private:

    ColumnBaseImp<CM> *m_columnBase = nullptr;

    std::string m_name;
    std::size_t m_size = 0u;
    ColumnarOffsetType *m_offsets = nullptr;
  };






  template<ObjectType O> class ReadObjectHandle<O,ColumnarModeExternal> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeExternal;

    ReadObjectHandle (ColumnBaseImp<CM>& columnBase,
                      const std::string& name)
      : m_columnBase (&columnBase),
        m_name (name)
    {
      columnBase.setObjectName (O, name);
      columnBase.addColumn<const ColumnarOffsetType> (name, m_offsetsIndex, O!=ObjectType::event ? numberOfEventsName : "", 1u);
    }

    template<typename U>
    ReadObjectHandle (U& owner,
                      const std::string& name,
                      const std::string& propertyName, const std::string& propertyTitle)
      : ReadObjectHandle (owner, name)
    {
    }

    ObjectRange<O,CM> getRange (ObjectId<ObjectType::event,CM> eventId) const
    {
      auto *offsets = static_cast<const ColumnarOffsetType*>(eventId.getData()[m_offsetsIndex]);
      return ObjectRange<O,CM> (eventId.getData(), offsets[eventId.getIndex()], offsets[eventId.getIndex() + 1]);
    }

    ObjectRange<O,CM> getRange (ObjectRange<ObjectType::event,CM> eventRange) const
    {
      auto *offsets = static_cast<const ColumnarOffsetType*>(eventRange.getData()[m_offsetsIndex]);
      return ObjectRange<O,CM> (eventRange.getData(), offsets[eventRange.beginIndex()], offsets[eventRange.endIndex()]);
    }

    const std::string& name () const noexcept
    {
      return m_name;
    }

    unsigned offsetsIndex () const noexcept {
      return m_offsetsIndex;}

    ColumnBaseImp<CM> *columnBase () noexcept {
      return m_columnBase;}



    /// Private Members
    /// ===============
  private:

    ColumnBaseImp<CM> *m_columnBase = nullptr;

    std::string m_name;
    unsigned m_offsetsIndex = 0;
  };





  using EventHandle = ReadObjectHandle<ObjectType::event>;
  using MuonHandle = ReadObjectHandle<ObjectType::muon>;
  using ElectronHandle = ReadObjectHandle<ObjectType::electron>;
  using ClusterHandle = ReadObjectHandle<ObjectType::cluster>;
}

#endif
