/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_RANGE_H
#define COLUMNAR_PROTOTYPE_OBJECT_RANGE_H

#include <ColumnarCore/ObjectType.h>
#include <ColumnarCore/ObjectId.h>
#include <exception>

namespace col
{
  /// @brief a class representing a continuous sequence of objects (a.k.a. a container)
  template<ObjectType OT,typename CM=ColumnarModeDefault> class ObjectRange;

  template<ObjectType OT,typename CM> class ObjectRangeIterator;





  template<ObjectType OT> class ObjectRange<OT,ColumnarModeXAOD> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODContainer = const typename detail::ObjectTypeTraits<OT>::xAODContainer;
    using CM = ColumnarModeXAOD;

    ObjectRangeIterator<OT,CM> begin () const noexcept {
      return ObjectRangeIterator<OT,CM> (this, m_beginIndex);}
    ObjectRangeIterator<OT,CM> end () const noexcept {
      return ObjectRangeIterator<OT,CM> (this, m_endIndex);}

    [[nodiscard]] std::size_t beginIndex () const noexcept {
      return m_beginIndex;}
    [[nodiscard]] std::size_t endIndex () const noexcept {
      return m_endIndex;}

    ObjectRange (xAODContainer *val_container) noexcept
      : m_container (val_container),
        m_beginIndex (0)
    {
      if constexpr (OT == ObjectType::event)
        m_endIndex = 1;
      else
        m_endIndex = m_container->size();
    }

    ObjectRange (xAODContainer *val_container, std::size_t val_beginIndex, std::size_t val_endIndex) noexcept
      : m_container (val_container),
        m_beginIndex (val_beginIndex), m_endIndex (val_endIndex)
    {}

    [[nodiscard]] xAODContainer *getContainer () const noexcept {
      return m_container;}


    [[nodiscard]] ObjectId<OT,CM> operator [] (std::size_t index) const noexcept {
      if constexpr (OT == ObjectType::event)
        return ObjectId<OT,CM> (*m_container);
      else
        return ObjectId<OT,CM> ((*m_container)[index]);
    }



    /// Private Members
    /// ===============
  private:

    xAODContainer *m_container = nullptr;
    std::size_t m_beginIndex = 0u;
    std::size_t m_endIndex = 0u;
  };





  template<ObjectType OT> class ObjectRange<OT,ColumnarModeDirect> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODContainer = typename detail::ObjectTypeTraits<OT>::xAODContainer;
    using CM = ColumnarModeDirect;

    ObjectRangeIterator<OT,CM> begin () const noexcept {
      return ObjectRangeIterator<OT,CM> (this, m_beginIndex);}
    ObjectRangeIterator<OT,CM> end () const noexcept {
      return ObjectRangeIterator<OT,CM> (this, m_endIndex);}

    [[nodiscard]] std::size_t beginIndex () const noexcept {
      return m_beginIndex;}
    [[nodiscard]] std::size_t endIndex () const noexcept {
      return m_endIndex;}

    ObjectRange (xAODContainer * /*val_container*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectRange (xAODContainer * /*val_container*/, std::size_t /*val_beginIndex*/, std::size_t /*val_endIndex*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    [[nodiscard]] xAODContainer *getContainer () const {
      throw std::logic_error ("can't call xAOD function in columnar mode");}

    [[nodiscard]] ObjectId<OT,CM> operator [] (std::size_t index) const noexcept {
      return ObjectId<OT,CM> (index);
    }



    /// Mode-Specific Public Members
    /// ============================
  public:

    explicit ObjectRange (std::size_t val_beginIndex,
                          std::size_t val_endIndex) noexcept
      : m_beginIndex (val_beginIndex), m_endIndex (val_endIndex)
    {}



    /// Private Members
    /// ===============
  private:

    std::size_t m_beginIndex = 0u;
    std::size_t m_endIndex = 0u;
  };





  template<ObjectType OT> class ObjectRange<OT,ColumnarModeExternal> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODContainer = typename detail::ObjectTypeTraits<OT>::xAODContainer;
    using CM = ColumnarModeExternal;

    ObjectRangeIterator<OT,CM> begin () const noexcept {
      return ObjectRangeIterator<OT,CM> (this, m_beginIndex);}
    ObjectRangeIterator<OT,CM> end () const noexcept {
      return ObjectRangeIterator<OT,CM> (this, m_endIndex);}

    [[nodiscard]] std::size_t beginIndex () const noexcept {
      return m_beginIndex;}
    [[nodiscard]] std::size_t endIndex () const noexcept {
      return m_endIndex;}

    ObjectRange (xAODContainer * /*val_container*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectRange (xAODContainer * /*val_container*/, std::size_t /*val_beginIndex*/, std::size_t /*val_endIndex*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    [[nodiscard]] xAODContainer *getContainer () const {
      throw std::logic_error ("can't call xAOD function in columnar mode");}

    [[nodiscard]] ObjectId<OT,CM> operator [] (std::size_t index) const noexcept {
      return ObjectId<OT,CM> (m_data, index);
    }



    /// Mode-Specific Public Members
    /// ============================
  public:

    explicit ObjectRange (void **val_data, std::size_t val_beginIndex,
                          std::size_t val_endIndex) noexcept
      : m_data (val_data), m_beginIndex (val_beginIndex), m_endIndex (val_endIndex)
    {}

    [[nodiscard]] void **getData () const noexcept {
      return m_data;}



    /// Private Members
    /// ===============
  private:

    void **m_data = nullptr;
    std::size_t m_beginIndex = 0u;
    std::size_t m_endIndex = 0u;
  };



  /// @brief an iterator over objects in an @ref ObjectRange
  ///
  /// This is primarily to allow the use of range-for for ObjectRange

  template<ObjectType OT,typename CM> class ObjectRangeIterator final
  {
  public:

    ObjectRangeIterator (const ObjectRange<OT,CM> *val_range,
                         std::size_t val_index) noexcept
      : m_range (val_range), m_index (val_index) {}

    ObjectId<OT,CM> operator * () const noexcept {
      return (*m_range)[m_index];
    }

    ObjectRangeIterator<OT,CM>& operator ++ () noexcept {
      ++ m_index; return *this;}

    bool operator == (const ObjectRangeIterator<OT,CM>& that) const noexcept {
      return m_index == that.m_index;}
    bool operator != (const ObjectRangeIterator<OT,CM>& that) const noexcept {
      return m_index != that.m_index;}

  private:
    const ObjectRange<OT,CM> *m_range = nullptr;
    std::size_t m_index = 0u;
  };

  using EventRange = ObjectRange<ObjectType::event>;
  using ElectronRange = ObjectRange<ObjectType::electron>;
}

#endif
