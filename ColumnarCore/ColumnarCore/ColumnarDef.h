/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef COLUMNAR_CORE_COLUMNAR_DEF_H
#define COLUMNAR_CORE_COLUMNAR_DEF_H

namespace col
{
  // This checks that COLUMNAR_ACCESS_MODE is indeed defined, plus makes it
  // available for use with `if constexpr`.
  constexpr unsigned columnarAccessMode = COLUMNAR_ACCESS_MODE;

  struct ColumnarModeXAOD {};
  struct ColumnarModeDirect {};
  struct ColumnarModeExternal {};

#if COLUMNAR_ACCESS_MODE == 0
  using ColumnarModeDefault = ColumnarModeXAOD;
#elif COLUMNAR_ACCESS_MODE == 1
  using ColumnarModeDefault = ColumnarModeDirect;
#elif COLUMNAR_ACCESS_MODE == 2
  using ColumnarModeDefault = ColumnarModeExternal;
#else
  #error "COLUMNAR_ACCESS_MODE must be 0, 1 or 2"
#endif
}

#endif
