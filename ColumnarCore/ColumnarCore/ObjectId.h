/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_ID_H
#define COLUMNAR_PROTOTYPE_OBJECT_ID_H

#include <ColumnarCore/ObjectType.h>
#include <exception>

namespace col
{
  /// @brief a class representing a single object (electron, muons, etc.)
  template<ObjectType O, typename CM = ColumnarModeDefault> class ObjectId;





  template<ObjectType O> class ObjectId<O,ColumnarModeXAOD> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODObject = const typename detail::ObjectTypeTraits<O>::xAODObject;

    ObjectId (xAODObject *val_object) noexcept
      : m_object (val_object)
    {}

    ObjectId (xAODObject& val_object) noexcept
      : m_object (&val_object)
    {}

    ObjectId (const ObjectId<O,ColumnarModeXAOD>& that) noexcept
      : m_object (that.getObject()) {}

    [[nodiscard]] xAODObject *getObject () const noexcept {
      return m_object;}



    /// Private Members
    /// ===============
  private:

    xAODObject *m_object = nullptr;
  };





  template<ObjectType O> class ObjectId<O,ColumnarModeDirect> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODObject = const typename detail::ObjectTypeTraits<O>::xAODObject;

    ObjectId (xAODObject * /*val_object*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectId (xAODObject& /*val_object*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectId (const ObjectId<O,ColumnarModeDirect>& that) noexcept
      : m_index (that.getIndex()) {}

    [[nodiscard]] xAODObject *getObject () const {
      throw std::logic_error ("can't call xAOD function in columnar mode");}



    /// Mode-Specific Public Members
    /// ============================
  public:

    explicit ObjectId (int val_index) noexcept
      : m_index (val_index)
    {}

    explicit ObjectId (unsigned val_index) noexcept
      : m_index (val_index)
    {}

    explicit ObjectId (std::size_t val_index) noexcept
      : m_index (val_index)
    {}

    [[nodiscard]] std::size_t getIndex () const noexcept {
      return m_index;}



    /// Private Members
    /// ===============
  private:

    std::size_t m_index = 0u;
  };





  template<ObjectType O> class ObjectId<O,ColumnarModeExternal> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODObject = const typename detail::ObjectTypeTraits<O>::xAODObject;

    ObjectId (xAODObject * /*val_object*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectId (xAODObject& /*val_object*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectId (const ObjectId<O,ColumnarModeExternal>& that) noexcept
      : m_data (that.m_data), m_index (that.getIndex()) {}

    [[nodiscard]] xAODObject *getObject () const {
      throw std::logic_error ("can't call xAOD function in columnar mode");}



    /// Mode-Specific Public Members
    /// ============================
  public:

    explicit ObjectId (void **val_data, int val_index) noexcept
      : m_data (val_data), m_index (val_index)
    {}

    explicit ObjectId (void **val_data, unsigned val_index) noexcept
      : m_data (val_data), m_index (val_index)
    {}

    explicit ObjectId (void **val_data, std::size_t val_index) noexcept
      : m_data (val_data), m_index (val_index)
    {}

    [[nodiscard]] std::size_t getIndex () const noexcept {
      return m_index;}

    [[nodiscard]] void **getData () const noexcept {
      return m_data;}



    /// Private Members
    /// ===============
  private:

    void **m_data = nullptr;
    std::size_t m_index = 0u;
  };





  using MuonId = ObjectId<ObjectType::muon>;
  using EventId = ObjectId<ObjectType::event>;
  using ElectronId = ObjectId<ObjectType::electron>;
  using ClusterId = ObjectId<ObjectType::cluster>;
}

#endif
