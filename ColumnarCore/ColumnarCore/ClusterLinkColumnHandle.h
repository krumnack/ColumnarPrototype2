/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_CLUSTER_LINK_COLUMN_HANDLE_H
#define COLUMNAR_PROTOTYPE_CLUSTER_LINK_COLUMN_HANDLE_H

#include <AthContainers/AuxElement.h>
#include "AthLinks/ElementLink.h"
#include <ColumnarCore/ObjectId.h>
#include <ColumnarCore/ObjectClusterLink.h>
#include <ColumnarCore/ObjectType.h>
#include <ColumnarCore/ReadObjectHandle.h>

#include <Eigen/Dense>

namespace col
{
  template<ObjectType OT,ObjectType LT,typename CM=ColumnarModeDefault> class ClusterLinkColumnHandle;





  template<ObjectType OT,ObjectType LT> class ClusterLinkColumnHandle<OT,LT,ColumnarModeXAOD> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeXAOD;

    ClusterLinkColumnHandle (ColumnBaseImp<CM>& /*columnBase*/,
                             const std::string& name)
      : m_accessor(name)
    {
    }

    ObjectClusterLink<LT,CM> operator [] (ObjectId<OT,CM> id) const noexcept
    {
      return ObjectClusterLink<LT,CM> (&m_accessor(*id.getObject()));
    }



    /// Private Members
    /// ===============

  private:

    typename SG::AuxElement::ConstAccessor<std::vector<ElementLink<typename detail::ObjectTypeTraits<LT>::xAODContainer>>> m_accessor;
  };





  template<ObjectType OT,ObjectType LT> class ClusterLinkColumnHandle<OT,LT,ColumnarModeDirect> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeDirect;

    ClusterLinkColumnHandle (ColumnBaseImp<CM>& columnBase,
                             const std::string& name)
    {
      columnBase.addColumn (columnBase.objectName(OT) + "_" + name + "_offset", m_offsetSize, m_offset, columnBase.objectName(OT), 1u);
      columnBase.addColumn (columnBase.objectName(OT) + "_" + name + "_data", m_dataSize, m_data, columnBase.objectName(OT) + "_" + name + "_offset");
    }


    ObjectClusterLink<LT,CM> operator [] (ObjectId<OT,CM> id) const noexcept
    {
      return ObjectClusterLink<LT,CM> (m_offset[id.getIndex()+1]-m_offset[id.getIndex()], &m_data[m_offset[id.getIndex()]]);
    }



    /// Private Members
    /// ===============

  private:

    ColumnarOffsetType m_offsetSize = 0u;
    ColumnarOffsetType *m_offset = nullptr;
    ColumnarOffsetType m_dataSize = 0u;
    ColumnarOffsetType *m_data = nullptr;
  };





  template<ObjectType OT,ObjectType LT> class ClusterLinkColumnHandle<OT,LT,ColumnarModeExternal> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeExternal;

    ClusterLinkColumnHandle (ColumnBaseImp<CM>& columnBase,
                             const std::string& name)
    {
      columnBase.addColumn<ColumnarOffsetType> (columnBase.objectName(OT) + "." + name + ".offset", m_offsetIndex, columnBase.objectName(OT), 1u);
      columnBase.addColumn<ColumnarOffsetType> (columnBase.objectName(OT) + "." + name + ".data", m_dataIndex, columnBase.objectName(OT) + "." + name + ".offset");
    }


    ObjectClusterLink<LT,CM> operator [] (ObjectId<OT,CM> id) const noexcept
    {
      auto *offset = static_cast<const ColumnarOffsetType*>(id.getData()[m_offsetIndex]);
      auto *data = static_cast<const ColumnarOffsetType*>(id.getData()[m_dataIndex]);
      return ObjectClusterLink<LT,CM> (id.getData(), offset[id.getIndex()+1]-offset[id.getIndex()], &data[offset[id.getIndex()]]);
    }



    /// Private Members
    /// ===============

  private:

    unsigned m_offsetIndex = 0u;
    unsigned m_dataIndex = 0u;
  };
}

#endif
