/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_RETYPE_COLUMN_HANDLE_H
#define COLUMNAR_PROTOTYPE_RETYPE_COLUMN_HANDLE_H

#include <AthContainers/AuxElement.h>
#include <ColumnarCore/ColumnBase.h>
#include <ColumnarCore/ObjectId.h>
#include <ColumnarCore/ObjectRange.h>
#include <ColumnarCore/ObjectType.h>
#include <ColumnarCore/ReadObjectHandle.h>

#include <Eigen/Dense>

namespace col
{
  template<ObjectType OT,typename UT,typename CT,typename CM> class RetypeColumnHandle final
  {
  public:

    RetypeColumnHandle (ColumnBaseImp<CM>& columnBase, const std::string& name)
      : m_base (columnBase, name)
    {}

    template<bool isMutable>
    RetypeColumnHandle (ReadObjectHandle<OT,CM>& objectHandle, const std::string& name)
      : m_base (objectHandle, name)
    {}

    template<bool isMutable>
    RetypeColumnHandle (ColumnBaseImp<CM>& owner, ReadObjectHandle<OT,CM>& objectHandle,
          const std::string& name)
      : m_base (owner, objectHandle, name)
    {}

    template<bool isMutable>
    UT operator [] (ObjectId<OT,CM> id) const noexcept
    {
      return UT (m_base[id]);
    }

    template<bool isMutable>
    UT operator () (ObjectId<OT,CM> id) const noexcept
    {
      return UT (m_base(id));
    }

  private:
    ColumnHandle<OT,CT,CM> m_base;
  };
}

#endif
