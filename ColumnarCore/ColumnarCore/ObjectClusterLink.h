/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_RANGE_VIEW_H
#define COLUMNAR_PROTOTYPE_OBJECT_RANGE_VIEW_H

#include <ColumnarCore/ObjectType.h>
#include <ColumnarCore/ObjectId.h>

namespace col
{
  template<ObjectType OT,typename CM=ColumnarModeDefault> class ObjectClusterLink;





  template<ObjectType OT> class ObjectClusterLink<OT,ColumnarModeXAOD> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeXAOD;

    ObjectClusterLink (const std::vector<ElementLink<typename detail::ObjectTypeTraits<OT>::xAODContainer>> *val_container) noexcept
      : m_container (val_container)
    {}

    [[nodiscard]] const std::vector<ElementLink<typename detail::ObjectTypeTraits<OT>::xAODContainer>> *getContainer () const noexcept {
      return m_container;}

    [[nodiscard]] ColumnarOffsetType size () const noexcept {
      return m_container->size();}

    [[nodiscard]] ObjectId<OT,CM> operator[] (ColumnarOffsetType index) const noexcept {
      return ObjectId<OT,CM>(*m_container->at(index));}



    /// Private Members
    /// ===============
  private:

    const std::vector<ElementLink<typename detail::ObjectTypeTraits<OT>::xAODContainer>> *m_container = nullptr;
  };





  template<ObjectType OT> class ObjectClusterLink<OT,ColumnarModeDirect> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeDirect;

    explicit ObjectClusterLink (ColumnarOffsetType val_size,
                                const ColumnarOffsetType *val_data) noexcept
      : m_size (val_size), m_data (val_data)
    {}

    [[nodiscard]] ColumnarOffsetType size () const noexcept {
      return m_size;}

    [[nodiscard]] ObjectId<OT,CM> operator[] (ColumnarOffsetType index) const noexcept {
      assert (index < m_size);
      return ObjectId<OT,CM> (m_data[index]);}



    /// Private Members
    /// ===============
  private:

    ColumnarOffsetType m_size = 0u;
    const ColumnarOffsetType *m_data = nullptr;
  };





  template<ObjectType OT> class ObjectClusterLink<OT,ColumnarModeExternal> final
  {
    /// Common Public Members
    /// =====================
  public:

    using CM = ColumnarModeExternal;

    explicit ObjectClusterLink (void **val_data, ColumnarOffsetType val_size,
                                const ColumnarOffsetType *val_columnData) noexcept
      : m_data (val_data), m_size (val_size), m_columnData (val_columnData)
    {}

    [[nodiscard]] ColumnarOffsetType size () const noexcept {
      return m_size;}

    [[nodiscard]] ObjectId<OT,CM> operator[] (ColumnarOffsetType index) const noexcept {
      assert (index < m_size);
      return ObjectId<OT,CM> (m_data, m_columnData[index]);}



    /// Private Members
    /// ===============
  private:

    void **m_data = nullptr;
    ColumnarOffsetType m_size = 0u;
    const ColumnarOffsetType *m_columnData = nullptr;
  };
}

#endif
