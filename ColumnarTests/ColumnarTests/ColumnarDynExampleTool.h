/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMNAR_DYN_EXAMPLE_TOOL_H
#define COLUMNAR_PROTOTYPE_COLUMNAR_DYN_EXAMPLE_TOOL_H

#include <AsgTools/AsgTool.h>
#include <AsgTools/PropertyWrapper.h>
#include <ColumnarCore/ColumnHandle.h>
#include <ColumnarCore/ColumnBase.h>
#include <ColumnarCore/ReadObjectHandle.h>
#include <optional>

namespace col
{
  class ColumnarDynExampleTool final
    : public asg::AsgTool,
      public ColumnBase
  {
  public:

    ColumnarDynExampleTool (const std::string& name);

    StatusCode initialize () override;

    /// do a calculation on a single object
    void calculateObject (ObjectId<ObjectType::muon> muon) const;

    /// call the above in an execute-style function
    void executeObjectLoop (EventRange events) const;

    /// call the above in an algorithm-execute-style function
    void executeEventsLoop (EventRange events) const;

    void executeEvents (EventRange events) const;

    ReadObjectHandle<ObjectType::event> m_events {*this, numberOfEventsName};
    ReadObjectHandle<ObjectType::muon> m_muons {*this, "Muons"};
    ColumnHandle<ObjectType::muon,const float> m_input1 {*this, "input1"};
    Gaudi::Property<std::string> m_input2Name {this, "input2", "", "input2 column name"};
    std::optional<ColumnHandle<ObjectType::muon,const float>> m_input2;
    ColumnHandle<ObjectType::muon,float> m_output {*this, "output"};
  };
}

#endif
