/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMNAR_EXAMPLE_TOOL_H
#define COLUMNAR_PROTOTYPE_COLUMNAR_EXAMPLE_TOOL_H

#include <AsgTools/AsgTool.h>
#include <ColumnarCore/ColumnHandle.h>
#include <ColumnarCore/ColumnBase.h>
#include <ColumnarCore/ReadObjectHandle.h>

namespace col
{
  class ColumnarExampleTool final
    : public asg::AsgTool,
      public ColumnBase
  {
  public:

    ColumnarExampleTool (const std::string& name);

    StatusCode initialize () override;

    /// do a calculation on a single object
    void calculateObject (ObjectId<ObjectType::muon> muon) const;

    /// do a calculation on a range of objects using a range-based for-loop
    void calculateRange (ObjectRange<ObjectType::muon> muon) const;

    /// do a calculation on a range of objects using vectorized data access
    void calculateVector (ObjectRange<ObjectType::muon> muons) const;

    /// call the above in an execute-style function
    void executeObjectLoop (EventRange events) const;
    void executeRange (EventRange events) const;
    void executeVector (EventRange events) const;

    /// call the above in an algorithm-execute-style function
    void executeEventsLoop (EventRange events) const;
    void executeEventsRange (EventRange events) const;

    void callEvents (EventRange events) const override;

    ReadObjectHandle<ObjectType::event> m_events {*this, "EventInfo"};
    ReadObjectHandle<ObjectType::muon> m_muons {*this, "Muons"};
    ColumnHandle<ObjectType::muon,const float> m_pt {*this, "pt"};
    ColumnHandle<ObjectType::muon,const float> m_eta {*this, "eta"};
    ColumnHandle<ObjectType::muon,float> m_output {*this, "output"};
  };
}

#endif
