/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarTests/ColumnarExampleTool.h>

#include <cmath>

//
// method implementations
//

namespace col
{
  ColumnarExampleTool ::
  ColumnarExampleTool ([[maybe_unused]] const std::string& name)
    : AsgTool (name)
  {}



  StatusCode ColumnarExampleTool ::
  initialize ()
  {
    ANA_CHECK (initializeColumns());
    return StatusCode::SUCCESS;
  }



  void ColumnarExampleTool ::
  calculateObject (ObjectId<ObjectType::muon> muon) const
  {
    m_output[muon] = m_pt[muon] * cosh(m_eta[muon]);
  }



  void ColumnarExampleTool ::
  calculateRange (ObjectRange<ObjectType::muon> muons) const
  {
    for (ObjectId<ObjectType::muon> muon : muons)
      m_output[muon] = m_pt[muon] * cosh(m_eta[muon]);
  }



  void ColumnarExampleTool ::
  calculateVector (ObjectRange<ObjectType::muon> muons) const
  {
    auto pt = m_pt[muons];
    auto eta = m_eta[muons];
    auto output = m_output[muons];
    for (decltype(pt)::Index index = 0; index != pt.size(); ++ index)
      output[index] = pt[index] * cosh(eta[index]);
  }



  void ColumnarExampleTool ::
  executeObjectLoop (EventRange events) const
  {
    for (ObjectId<ObjectType::muon> muon : m_muons.getRange(events))
      calculateObject (muon);
  }

  void ColumnarExampleTool ::
  executeRange (EventRange events) const
  {
    auto range = m_muons.getRange(events);

    calculateRange (range);
  }

  void ColumnarExampleTool ::
  executeVector (EventRange events) const
  {
    calculateVector (m_muons.getRange(events));
  }



  void ColumnarExampleTool ::
  executeEventsLoop (EventRange events) const
  {
    for (ObjectId<ObjectType::event> event : events)
    {
      calculateRange (m_muons.getRange(event));
    }
  }

  void ColumnarExampleTool ::
  executeEventsRange (EventRange events) const
  {
    calculateRange (m_muons.getRange(events));
  }



  void ColumnarExampleTool ::
  callEvents (EventRange events) const
  {
    calculateRange (m_muons.getRange(events));
  }
}
