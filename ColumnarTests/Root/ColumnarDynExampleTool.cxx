/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarTests/ColumnarDynExampleTool.h>

#include <cmath>

//
// method implementations
//

namespace col
{
  ColumnarDynExampleTool ::
  ColumnarDynExampleTool ([[maybe_unused]] const std::string& name)
    : AsgTool (name)
  {}



  StatusCode ColumnarDynExampleTool ::
  initialize ()
  {
    if (!m_input2Name.value().empty())
      m_input2.emplace (*this, m_input2Name.value());
    ANA_CHECK (initializeColumns());
    return StatusCode::SUCCESS;
  }



  void ColumnarDynExampleTool ::
  calculateObject (ObjectId<ObjectType::muon> muon) const
  {
    m_output[muon] = m_input1[muon];
    if (m_input2.has_value())
      m_output[muon] += m_input2.value()[muon];
  }



  void ColumnarDynExampleTool ::
  executeObjectLoop (EventRange events) const
  {
    for (ObjectId<ObjectType::muon> muon : m_muons.getRange(events))
      calculateObject (muon);
  }



  void ColumnarDynExampleTool ::
  executeEventsLoop (EventRange events) const
  {
    for (ObjectId<ObjectType::event> event : m_events.getRange(events))
    {
      for (ObjectId<ObjectType::muon> muon : m_muons.getRange(event))
        calculateObject (muon);
    }
  }



  void ColumnarDynExampleTool ::
  executeEvents (EventRange events) const
  {
    for (ObjectId<ObjectType::muon> muon : m_muons.getRange(events))
      calculateObject (muon);
  }
}
