/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarTests/ColumnarLinkExampleTool.h>

#include <cmath>

//
// method implementations
//

namespace col
{
  ColumnarLinkExampleTool ::
  ColumnarLinkExampleTool ([[maybe_unused]] const std::string& name)
    : AsgTool (name)
  {}



  StatusCode ColumnarLinkExampleTool ::
  initialize ()
  {
    ATH_CHECK (initializeColumns());
    return StatusCode::SUCCESS;
  }



  void ColumnarLinkExampleTool ::
  calculateObject (ObjectId<ObjectType::electron> electron) const
  {
    decltype(auto) clusters = m_cluster_links[electron];
    if (clusters.size() > 0)
    {
      m_output[electron] = m_pt[electron] * cosh(m_calEta[clusters[0]]);
    } else
    {
      m_output[electron] = 0;
    }
  }



  void ColumnarLinkExampleTool ::
  callEvents (EventRange events) const
  {
    for (ObjectId<ObjectType::electron> electron : m_electrons.getRange(events))
      calculateObject (electron);
  }
}
