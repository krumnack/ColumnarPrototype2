/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include "CxxUtils/checker_macros.h"
ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

#include <AsgTesting/UnitTest.h>
#include <AsgTools/AsgToolConfig.h>
#include <ColumnarToolWrappers/PythonToolWrapper.h>
#include <ColumnarTests/ColumnarDynExampleTool.h>
#include <ColumnarTests/ColumnarExampleTool.h>
#include <ColumnarTests/ColumnarLinkExampleTool.h>
#include <gtest/gtest.h>
#include <gtest/gtest-spi.h>

#include <TInterpreter.h>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

//
// method implementations
//

namespace col
{
  namespace
  {
    /// \brief make a unique tool name to be used in unit tests
    std::string makeUniqueName ()
    {
      static std::atomic<unsigned> index = 0;
      return "unique" + std::to_string(++index);
    }
  }

  TEST (ColumnarMode2Test, getColumn)
  {
    auto tool = std::make_shared<ColumnarExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (1);
    data.setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    eta.push_back (1);
    output.resize (pt.size(), 0.);
    data.setColumn ("Muons.pt", pt.size(), pt.data());
    data.setColumn ("Muons.eta", eta.size(), eta.data());
    data.setColumn ("Muons.output", output.size(), output.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);
    data.setColumn (numberOfEventsName, numEvents.size(), numEvents.data());
    data.checkColumnsValid ();

    EXPECT_EQ (pt.size(), data.getColumn<const float>("Muons.pt").first);
    EXPECT_EQ (pt.data(), data.getColumn<const float>("Muons.pt").second);
    EXPECT_EQ (output.size(), data.getColumn<float>("Muons.output").first);
    EXPECT_EQ (output.data(), data.getColumn<float>("Muons.output").second);
  }

  TEST (ColumnarMode2Test, object)
  {
    auto tool = std::make_shared<ColumnarExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (1);
    data.setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    eta.push_back (1);
    output.resize (pt.size(), 0.);
    data.setColumn ("Muons.pt", pt.size(), pt.data());
    data.setColumn ("Muons.eta", eta.size(), eta.data());
    data.setColumn ("Muons.output", output.size(), output.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);
    data.setColumn (numberOfEventsName, numEvents.size(), numEvents.data());
    data.checkColumnsValid ();
    tool->calculateObject (ObjectId<ObjectType::muon>(data.data(), 0));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
  }



  TEST (ColumnarMode2Test, range)
  {
    auto tool = std::make_shared<ColumnarExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    data.setColumn ("Muons.pt", pt.size(), pt.data());
    data.setColumn ("Muons.eta", eta.size(), eta.data());
    data.setColumn ("Muons.output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    data.setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);
    data.setColumn (numberOfEventsName, numEvents.size(), numEvents.data());
    data.checkColumnsValid ();
    tool->calculateRange (ObjectRange<ObjectType::muon>(data.data(), 0, offsets.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode2Test, vector)
  {
    auto tool = std::make_shared<ColumnarExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    data.setColumn ("Muons.pt", pt.size(), pt.data());
    data.setColumn ("Muons.eta", eta.size(), eta.data());
    data.setColumn ("Muons.output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    data.setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);
    data.setColumn (numberOfEventsName, numEvents.size(), numEvents.data());
    data.checkColumnsValid ();
    tool->calculateVector (ObjectRange<ObjectType::muon>(data.data(), 0, offsets.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode2Test, execute)
  {
    auto tool = std::make_shared<ColumnarExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    data.setColumn ("Muons.pt", pt.size(), pt.data());
    data.setColumn ("Muons.eta", eta.size(), eta.data());
    data.setColumn ("Muons.output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    data.setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);
    data.setColumn (numberOfEventsName, numEvents.size(), numEvents.data());
    data.checkColumnsValid ();
    tool->executeRange (EventRange(data.data(), 0, numEvents.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode2Test, dyn_column)
  {
    auto tool = std::make_shared<ColumnarDynExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<float> input1, input2, output;
    input1.push_back (1);
    input1.push_back (2);
    input2.push_back (4);
    input2.push_back (3);
    output.resize (input1.size(), 0.);
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (input1.size());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);

    std::map<std::string, std::vector<float>*> float_columns;
    float_columns["Muons.input1"] = &input1;
    float_columns["Muons.inputOptional"] = &input2;
    float_columns["Muons.output"] = &output;
    std::map<std::string, std::vector<ColumnarOffsetType>*> offset_columns;
    offset_columns["Muons"] = &offsets;
    offset_columns[numberOfEventsName] = &numEvents;

    for (auto& column : data.getColumnNames())
    {
      if (auto iter = float_columns.find (column);
          iter != float_columns.end())
        data.setColumn (column, iter->second->size(), iter->second->data());
      else if (auto iter = offset_columns.find (column);
               iter != offset_columns.end())
        data.setColumn (column, iter->second->size(), iter->second->data());
      else
        throw std::logic_error ("unknown column: " + column);
    }
    data.checkColumnsValid ();

    tool->executeObjectLoop (EventRange(data.data(), 0, numEvents.back()));
    EXPECT_FLOAT_EQ (1, output[0]);
    EXPECT_FLOAT_EQ (2, output[1]);
  }



  TEST (ColumnarMode2Test, dyn_column2)
  {
    auto tool = std::make_shared<ColumnarDynExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->setProperty ("input2", "inputOptional"));
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<float> input1, input2, output;
    input1.push_back (1);
    input1.push_back (2);
    input2.push_back (4);
    input2.push_back (3);
    output.resize (input1.size(), 0.);
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (input1.size());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);

    std::map<std::string, std::vector<float>*> float_columns;
    float_columns["Muons.input1"] = &input1;
    float_columns["Muons.inputOptional"] = &input2;
    float_columns["Muons.output"] = &output;
    std::map<std::string, std::vector<ColumnarOffsetType>*> offset_columns;
    offset_columns["Muons"] = &offsets;
    offset_columns[numberOfEventsName] = &numEvents;

    for (auto& column : data.getColumnNames())
    {
      if (auto iter = float_columns.find (column);
          iter != float_columns.end())
        data.setColumn (column, iter->second->size(), iter->second->data());
      else if (auto iter = offset_columns.find (column);
               iter != offset_columns.end())
        data.setColumn (column, iter->second->size(), iter->second->data());
      else
        throw std::logic_error ("unknown column: " + column);
    }
    data.checkColumnsValid ();

    tool->executeObjectLoop (EventRange(data.data(), 0, numEvents.back()));
    EXPECT_FLOAT_EQ (5, output[0]);
    EXPECT_FLOAT_EQ (5, output[1]);
  }



  TEST (ColumnarMode2Test, element_link)
  {
    auto tool = std::make_shared<ColumnarLinkExampleTool> (makeUniqueName());
    ASSERT_SUCCESS (tool->initialize());
    PythonToolWrapper toolWrapper {tool};
    PythonToolData data {&toolWrapper};
    std::vector<float> calEta;
    calEta.push_back (0);
    calEta.push_back (1);
    calEta.push_back (2);
    calEta.push_back (3);
    calEta.push_back (4);
    calEta.push_back (5);
    calEta.push_back (6);
    data.setColumn ("egammaClusters.calEta", calEta.size(), calEta.data());
    std::vector<ColumnarOffsetType> caloClusterOffsets;
    caloClusterOffsets.push_back (0);
    caloClusterOffsets.push_back (calEta.size());
    data.setColumn ("egammaClusters", caloClusterOffsets.size(), caloClusterOffsets.data());

    std::vector<float> pt, output;
    std::vector<std::size_t> clusterLinkOffset;
    std::vector<std::size_t> clusterLinkData;
    pt.push_back (10e5);
    pt.push_back (20e5);
    clusterLinkOffset.push_back (0);
    clusterLinkOffset.push_back (2);
    clusterLinkOffset.push_back (5);
    clusterLinkData.push_back (0);
    clusterLinkData.push_back (1);
    clusterLinkData.push_back (2);
    clusterLinkData.push_back (3);
    clusterLinkData.push_back (0);
    output.resize (pt.size(), 0.);
    data.setColumn ("Electrons.pt", pt.size(), pt.data());
    data.setColumn ("Electrons.caloClusterLinks.offset", clusterLinkOffset.size(), clusterLinkOffset.data());
    data.setColumn ("Electrons.caloClusterLinks.data", clusterLinkData.size(), clusterLinkData.data());
    data.setColumn ("Electrons.output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    data.setColumn ("Electrons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (0);
    numEvents.push_back (offsets.size()-1);
    data.setColumn (numberOfEventsName, numEvents.size(), numEvents.data());
    data.checkColumnsValid ();
    tool->calculateObject (ObjectId<ObjectType::electron>(data.data(), 0));
    tool->calculateObject (ObjectId<ObjectType::electron>(data.data(), 1));

    EXPECT_FLOAT_EQ (10e5*cosh(0), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }
}

ATLAS_GOOGLE_TEST_MAIN
