/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include <xAODRootAccess/Init.h>
#include <AsgMessaging/MessageCheck.h>
#include <ColumnarToolWrappers/PythonToolWrapper.h>
#include <ColumnarTests/ColumnarExampleTool.h>
#include <TFile.h>

int main ()
{
  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE (int);

  ANA_CHECK (xAOD::Init());

  const std::string toolName = "MyTool";
  // asg::AsgToolConfig config ("col::ColumnarExampleTool/" + toolName);
  // std::shared_ptr<void> cleanup;
  // ToolHandle<ColumnarExampleTool> tool;
  // ASSERT_SUCCESS (config.makeTool (tool, cleanup));
  auto tool = std::make_unique<col::ColumnarExampleTool>(toolName);

  col::PythonToolWrapper toolWrapper {&*tool};
  col::PythonToolData data {&toolWrapper};
  std::vector<float> pt, eta, output;
  pt.push_back (10e5);
  pt.push_back (20e5);
  eta.push_back (1);
  eta.push_back (2);
  output.resize (pt.size(), 0.);
  data.setColumn ("Muons_pt", pt.size(), pt.data());
  data.setColumn ("Muons_eta", eta.size(), eta.data());
  data.setColumn ("Muons_output", output.size(), output.data());
  std::vector<col::ColumnarOffsetType> offsets;
  offsets.push_back (0);
  offsets.push_back (pt.size());
  data.setColumn ("Muons", offsets.size(), offsets.data());
  std::vector<col::ColumnarOffsetType> eventOffsets;
  eventOffsets.push_back (0);
  eventOffsets.push_back (offsets.size()-1);
  data.setColumn (col::numberOfEventsName, eventOffsets.size(), eventOffsets.data());
  data.checkColumnsValid ();
  tool->calculateVector (col::ObjectRange<col::ObjectType::muon>(data.data(), 0, offsets.back()));

  return 0;
}
