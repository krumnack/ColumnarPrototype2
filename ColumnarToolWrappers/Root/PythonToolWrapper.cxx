/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


//
// includes
//

#include <ColumnarToolWrappers/PythonToolWrapper.h>

#include <ColumnarInterfaces/ColumnInfo.h>
#include <ColumnarInterfaces/IColumnarTool.h>
#include <stdexcept>

//
// method implementations
//

namespace col
{
  PythonToolWrapper ::
  PythonToolWrapper (const IColumnarTool *val_tool)
    : m_tool (val_tool)
  {
    auto toolColumns = m_tool->getColumnInfo();
    for (auto& column : toolColumns)
    {
      MyColumnInfo myinfo;
      myinfo.index = column.index;
      myinfo.type = column.type;
      switch (column.accessMode)
      {
      case ColumnAccessMode::input:
        myinfo.isConst = true;
        break;
      case ColumnAccessMode::output:
        myinfo.isConst = false;
        break;
      }
      myinfo.isOffset = column.isOffset;

      auto [iter, success] = m_columns.emplace (column.name, std::move (myinfo));
      if (!success)
        throw std::runtime_error ("column name already registered: " + column.name);

      if (m_numColumns <= column.index)
        m_numColumns = column.index + 1;
   }

    for (auto& column : toolColumns)
    {
      if (!column.offsetName.empty())
      {
        auto offsetIter = m_columns.find (column.offsetName);
        if (offsetIter == m_columns.end())
          throw std::runtime_error ("offset column name not found: " + column.offsetName);
        if (*offsetIter->second.type != typeid (ColumnarOffsetType))
          throw std::runtime_error ("offset column has wrong type: " + column.offsetName);
        if (!offsetIter->second.isOffset)
          throw std::runtime_error ("offset column is not registered as offset: " + column.offsetName);
        m_columns.at (column.name).offsets = &*offsetIter;
      }
    }
  }



  PythonToolWrapper ::
  PythonToolWrapper (std::shared_ptr<const IColumnarTool> val_tool)
    : PythonToolWrapper (val_tool.get())
  {
    m_toolOwn = std::move (val_tool);
  }



  PythonToolData ::
  PythonToolData (const PythonToolWrapper *val_wrapper) noexcept
    : m_wrapper (val_wrapper),
      m_data (m_wrapper->m_numColumns, nullptr),
      m_dataSize (m_wrapper->m_numColumns, 0u),
      m_columnIsChecked (m_wrapper->m_numColumns, false)
  {}



  void PythonToolData ::
  setColumnVoid (const std::string& name, std::size_t size, const void *dataPtr, const std::type_info& type, bool isConst)
  {
    auto column = m_wrapper->m_columns.find (name);
    if (column == m_wrapper->m_columns.end())
      throw std::runtime_error ("unknown column name: " + name);

    if (type != *column->second.type)
      throw std::runtime_error ("invalid type for column: " + name);
    if (isConst && !column->second.isConst)
      throw std::runtime_error ("assigning const vector to a non-const column: " + name);
    m_data[column->second.index] = const_cast<void*>(dataPtr);
    m_dataSize[column->second.index] = size;
  }



  std::pair<std::size_t,const void*> PythonToolData ::
  getColumnVoid (const std::string& name, const std::type_info *type, bool isConst)
  {
    auto column = m_wrapper->m_columns.find (name);
    if (column == m_wrapper->m_columns.end())
      throw std::runtime_error ("unknown column name: " + name);

    if (*type != *column->second.type)
      throw std::runtime_error ("invalid type for column: " + name);
    if (!isConst && column->second.isConst)
      throw std::runtime_error ("retrieving non-const vector from a const column: " + name);
    if (m_data[column->second.index] != nullptr)
      return std::make_pair (m_dataSize[column->second.index],
                              m_data[column->second.index]);
    else
      return std::make_pair (0u, nullptr);
  }



  void PythonToolData ::
  checkColumnsValid ()
  {
    for (auto& column : m_wrapper->m_columns)
      checkColumn (column);
  }



  void PythonToolData ::
  call ()
  {
    checkColumnsValid ();
    m_wrapper->m_tool->callVoid (m_data.data());
  }



  void PythonToolData ::
  checkColumn (const std::pair<const std::string,PythonToolWrapper::MyColumnInfo>& column)
  {
    if (m_columnIsChecked.at(column.second.index))
      return;
    if (m_data.at(column.second.index) == nullptr)
      throw std::runtime_error ("column not filled: " + column.first);

    ColumnarOffsetType expectedSize = 1u;
    if (column.second.offsets)
    {
      checkColumn (*column.second.offsets);
      const auto offsetIndex = column.second.offsets->second.index;
      auto *offsetsPtr = static_cast<const ColumnarOffsetType*>(m_data[offsetIndex]);
      expectedSize = offsetsPtr[m_dataSize[offsetIndex]-1];
    }

    if (column.second.isOffset)
      expectedSize += 1u;

    if (m_dataSize[column.second.index] != expectedSize)
      throw std::runtime_error ("column size doesn't match expected size: " + column.first + ", found " + std::to_string (m_dataSize[column.second.index]) + " vs " + std::to_string (expectedSize));

    if (column.second.isOffset)
    {
      auto *dataPtr = static_cast<const ColumnarOffsetType*>(m_data[column.second.index]);
      if (dataPtr[0] != 0)
        throw std::runtime_error ("offset column doesn't start with 0: " + column.first);
    }

    m_columnIsChecked[column.second.index] = true;
  }



  std::vector<std::string> PythonToolData ::
  getColumnNames () const
  {
    std::vector<std::string> result;
    for (auto& column : m_wrapper->m_columns)
      result.push_back (column.first);
    return result;
  }



  [[nodiscard]] std::vector<ColumnInfo> PythonToolData ::
  getColumnInfo () const
  {
    return m_wrapper->m_tool->getColumnInfo();
  }
}
