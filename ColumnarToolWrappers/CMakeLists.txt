# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# @author Nils Krumnack

atlas_subdir( ColumnarToolWrappers )

# Add the shared library:
atlas_add_library (ColumnarToolWrappersLib
ColumnarToolWrappers/*.h Root/*.cxx
  PUBLIC_HEADERS ColumnarToolWrappers
  LINK_LIBRARIES ColumnarInterfacesLib)
